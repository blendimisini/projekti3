<?php include 'partial/header.php'?>
<?php


if( isset($_SESSION['id']) ){
    header("Location: index.php");
}

require 'includes/lidhjadb.php';

if(isset($_POST['submit'])){
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    $message = '';

    $query = $DB_con->prepare('SELECT id,name,email,password FROM user WHERE email = :email');

    $query->bindParam( ':email' , $email);
    $query->execute();

    $user = $query->fetch();

    if(count($user) > 0 && password_verify($password, $user['password']) ){

        $_SESSION['id'] = $user['id'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['autorizimi'] = 'kaqasje';
        header("Location: index.php");

        }   else {
        $_SESSION['autorizimi'] = 'skaqasje';
        $message = 'Te dhenat nuk mjaftojn !';
    }
}
?>
      <div id="opoplabi">
            <h1>Login</h1>
    <span>or <a href="singup.php">Signup here</a></span>

    <form action="includes/signin.php" method="POST">
        <input type="text" placeholder=" Email..." name="email"><br>
        <input type="password" placeholder="Password..." name="password"><br>
        <input type="submit" name="submit" value="Submit">
        <?php if(!empty($message)): ?>
        <p><?php echo $message ?></p>
    <?php endif; ?>
    </form>
    </div>
<?php include 'partial/footer.php' ?>