<?php
   
   session_start();
   ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Hope Center</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
</head>
<body id="page1">
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <div class="wrapper">
        <h1><a href="index.php" id="logo">Hope Center</a></h1>
        <nav>
          <ul id="top_nav">
            <li><a href="index.php"><img src="images/top_icon1.gif" alt=""></a></li>
            <li><a href="#"><img src="images/top_icon2.gif" alt=""></a></li>
            <li class="end"><a href="contact.php"><img src="images/top_icon3.gif" alt=""></a></li> 
          </ul>
        </nav>
        <nav>
          <ul id="menu">
            <li id="menu_active"><a href="index.php">Home</a></li>
            <li><a href="help.php">How to Help</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li>
            <?php 
                if(isset($_SESSION['id'])){
                    echo '<form action="includes/logout.php" method="POST"> 
                            <button type="submit" name="submit" class="logout-button" id="menu">Logout</button>
                          </form>
                          </li>';
                       
                        }
                            else{
                                echo '<a href="login.php">Log in</a></li>"';
                              }
                if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == '1'){
                  echo '<li><a href="admin.php">Users</a></li>';
              }
                ?>
            
          </ul>
        </nav>
      </div>
      <div class="slider">
        <ul class="items">
          <li> <img src="images/img1.jpg" alt="">
            <div class="banner">
              <div class="wrapper"><span>“Our<em>Mission</em>is to<em>Help</em></span></div>
              <div class="wrapper"><strong>Those Who<em>Need</em>It”</strong></div>
            </div>
          </li>
          <li> <img src="images/img2.jpg" alt="">
            <div class="banner">
              <div class="wrapper"><span>“MAKE all the CHILDREN</span></div>
              <div class="wrapper"><strong>of the World HAPPY”</strong></div>
            </div>
          </li>
          <li> <img src="images/img3.jpg" alt="">
            <div class="banner">
              <div class="wrapper"><span>“TOGETHER we can CHANGE</span></div>
              <div class="wrapper"><strong>Many Young LIVES”</strong></div>
            </div>
          </li>
        </ul>
        <ul class="pagination">
          <li id="banner1"><a href="#">Make<span>Donations</span></a></li>
          <li id="banner2"><a href="#">join<span>volunteer</span></a></li>
          <li id="banner3"><a href="#">Help<span>children</span></a></li>
        </ul>
      </div>
    </header>