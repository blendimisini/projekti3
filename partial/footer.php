<footer>
      <div class="wrapper"> <a href="index.html" id="footer_logo"><span>Hope</span>Center</a>
        <ul id="icons">
          <li><a href="#" class="normaltip"><img src="images/icon1.gif" alt=""></a></li>
          <li><a href="#" class="normaltip"><img src="images/icon2.gif" alt=""></a></li>
          <li><a href="#" class="normaltip"><img src="images/icon3.gif" alt=""></a></li>
        </ul>
      </div>
      <div class="wrapper">
        <nav>
          <ul id="footer_menu">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="help.php">How to Help</a></li>
            <li class="end"><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
        <div class="tel"><span>+1 800</span>123 45 67</div>
      </div>
     
    </footer>
  </div>
</div>  
</body>
</html>