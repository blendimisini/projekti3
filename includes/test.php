<?php
 
 include 'lidhjadb.php';

 $nameError = $emailError = $passwordError = $contryError = $birthdayError = "";
 $users = '';
 $successSignup = "";
 if($_SERVER["REQUEST_METHOD"] == "POST"){
     
     $name = $_POST['name'];
     $email = $_POST['email'];
     $gender = $_POST['gender'];
     $contry = $_POST['contry'];
     $birthday = $_POST['birthday'];
     $pwd = $_POST['password'];
     $pwd2 = $_POST['password2'];
     
     $query = $pdo->prepare("SELECT email FROM users WHERE email = :email");
     $query->execute([':email' => $email]);
     $users = $query->fetch();
     var_dump($users);
     if(!preg_match("/^[a-zA-Z]*$/", $name)){

        $nameError = "Only letters are allowed!";

     }else{
        if(empty($name)){
            $nameError = 'Empty field';
        }else{
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                $emailError = 'Enter a valid email';
            }else{
                if($users > 0 ){
                    $emailError = "Pordoruesi Egizston!";
                }else{
                    if(empty($_POST['birthday'])){
                        $birthdayError = 'Empty field';
                    }else{
                        if(empty($pwd) || empty($pwd2)){
                            $passwordError = 'Empty field';
                        }else{
                            if($pwd !== $pwd2){
                                $passwordError = 'Password doesnt match';
                            }else{
                                $password = password_hash($pwd, PASSWORD_DEFAULT);
                                $query = $pdo->prepare('INSERT INTO `users`(`name`, `email`, `gender`, `contry`, `birthday`, `password`) VALUES (:name, :email, :gender, :contry, :birthday,:password)');
             
                                $query->bindParam(':name',$name);
                                $query->bindParam(':email',$email);
                                $query->bindParam(':gender',$gender);
                                $query->bindParam(':contry',$contry);
                                $query->bindParam(':birthday',$birthday);
                                $query->bindParam(':password',$password);

                                $query->execute();


                                $successSignup = 'Succesfully Inserted';
                                $name = $email = $gender = $contry = $birthday = $pwd = $pwd2 = $users =  "";

                            }
                        }
                    }
                }
            }
        }
     }
 
    }
