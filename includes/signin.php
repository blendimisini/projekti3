<?php include 'lidhjadb.php' ?>

<?php
    if(isset($_SESSION['id'])){
        header('Location: ../index.php');
    }

?>

<?php
    if(isset($_POST['submit'])){
        $email = $_POST['email'];
        $password = $_POST['password'];

        $query = $pdo->prepare('SELECT * FROM users WHERE email = :email');
        
        $query->bindParam(':email',$email);
        $query->execute();

        $user = $query->fetch();

        if($user > 0 && password_verify($password, $user['password'])){

            session_start();
            $_SESSION['id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['is_admin'] = $user['is_admin'];
            
            header('Location: ../index.php?error=succesLogin');
        } else{
            header('Location: ../index.php?error=user-pw-wrong');
            exit();
            
        }
    }

?>
